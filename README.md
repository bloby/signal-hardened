# Signal Hardened

## Finding

Signal is a robust messenging application that has a strong reputation of security. Nevertheless, Signal Desktop stores its data transparently within user's files. Of course, any wise user has deployed  encrypted storage for its data. But what will happen if its computer (=its session) is found opened, or cracked? The intruder will find all its Signal data through its ``~/.config/Signal``` directory or similar. 

## Purpose

The idea here is to harden Signal storage system to have a better level of security.

We will use here a tool for directory encryption, which will decrypt Signal data when the user launches ```signal-desktop```, and which will encrypt back when not currently in use. 

## Getting started

### Requirements

- ```tor```
- ```encfs```
- ```zenity``` (Gnome Dialog utility)
- ```signal-desktop``` (find how to install Signal on your Linux Desktop on [signal.org](https://www.signal.org/download))

Under Debian-like Linux distributions (like Ubuntu), please do :

1. Install Tor : https://support.torproject.org/apt/tor-deb-repo/
2. Install ```encfs```, ```signal-desktop``` and ```zenity``` : ```$ sudo apt install encfs zenity signal-desktop```

### Installation, theory

Just download the ```molly-desktop``` script, in this repository, put it in a directory in your ```$PATH```, and make it executable, and the job is done.

### Installation, script

```bash
mkdir ~/bin
torify wget 'http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/signal-hardened/-/raw/no-masters/molly-desktop?ref_type=heads&inline=false' -O ~/bin/molly-desktop
chmod +x ~/bin/molly-desktop
export PATH=$PATH:~/bin
```

WARNING: to make this new $PATH persistent, please record it in your configuration files

### Desktop launcher (Gnome)

1. Install your ```molly-desktop``` script as seen previously
2. Download the ```molly-desktop.desktop``` launcher for Gnome
3. Modify the file to get the proper path to your ```molly-desktop``` script
4. Move the launcher in your ```~/.local/share/applications/``` directory
5. Done.

```bash
torify wget 'http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/signal-hardened/-/raw/no-masters/molly-desktop.desktop?inline=false' -O - | sed "s#~#$HOME#" > ~/.local/share/applications/molly-desktop.desktop
```

## Limits

Our testing platform uses encrypted home directories, using ```ecryptfs```. ```ecryptfs``` does not allow nested encryption (encrypting a directory within an encrypted directory). Because of that we choosed to use ```encfs``` which is known for its weaknesses. This choice has been done in the idea of "better than nothing". If any better idea comes to you, please let us know, or please submit a pull request!

We are still looking for easy-to-use solutions of deep-erasing the memory used by Signal after its exit. If any idea comes to you, let us know.

## TODO

1. Confirm the password creation to avoid keyboard errors
2. Add a feature that allows to change the password easily

## License

GNU GPL v3
